import * as cdk from '@aws-cdk/core';
import * as ec2 from "@aws-cdk/aws-ec2";
import * as ecs from "@aws-cdk/aws-ecs";
import * as ecs_patterns from "@aws-cdk/aws-ecs-patterns";
import * as rds from '@aws-cdk/aws-rds';
import * as cloudfront from '@aws-cdk/aws-cloudfront'
import * as origins  from '@aws-cdk/aws-cloudfront-origins'
import { Duration } from '@aws-cdk/core';



// import * as sqs from '@aws-cdk/aws-sqs';

export class CdkEcsAlbRoute53Stack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here

    const vpc = new ec2.Vpc(this, "MyVpc", {
      maxAzs: 2,  // Default is all AZs in region
      subnetConfiguration: [
        {
          cidrMask: 24,
          name: 'ingress',
          subnetType: ec2.SubnetType.PUBLIC,
        },
        {
          cidrMask: 24,
          name: 'rds-redis',
          subnetType: ec2.SubnetType.PRIVATE_ISOLATED,
        },
     ]
    });

    const cluster = new ecs.Cluster(this, "MyCluster", {
      vpc: vpc
    });

    const fargateSecurityGroup = new ec2.SecurityGroup(this, 'MySecurityGroup', { 
      vpc, 
      description: 'Allow  access to ecs task',
      allowAllOutbound: true   // Can be set to false
    });

    fargateSecurityGroup.addIngressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(80), 'allow http from the world');

    const rdsSecurityGroup = new ec2.SecurityGroup(this, 'MyRDSSecurityGroup', { 
      vpc, 
      description: 'Allow  access to ecs task',
      allowAllOutbound: true   // Can be set to false
    });

    rdsSecurityGroup.addIngressRule(fargateSecurityGroup, ec2.Port.tcp(3306), 'allow ecs task connect to rds');


    const fargate = new ecs_patterns.ApplicationLoadBalancedFargateService(this, "MyFargateService", {
      cluster: cluster, // Required
      cpu: 256, // Default is 256
      desiredCount: 1, // Default is 1
      taskImageOptions: { 
        image: ecs.ContainerImage.fromRegistry("amazon/amazon-ecs-sample") 
        // image: ecs.ContainerImage.fromAsset('./code') , 
        // environment : {
        //   USER: 'admin' , 
        //   PASS: 'ysvv9h-oQ5pCqNMBSSZGDRJ2=Pi-.G' , 
        //   ENDPOINT: 'ci3xw9juyeuik6.cav3bnn0mduw.ap-southeast-1.rds.amazonaws.com' , 
        // }
      },
      memoryLimitMiB: 512, // Default is 512
      publicLoadBalancer: true,  // Default is false
      assignPublicIp: true, 
      securityGroups: [ fargateSecurityGroup] , 
    });

    const myOriginRequestPolicy = new cloudfront.OriginRequestPolicy(this, 'OriginRequestPolicy', {
      originRequestPolicyName: 'MyPolicy',
      comment: 'A default policy',
      headerBehavior: cloudfront.OriginRequestHeaderBehavior.all(), 
    });

    //bind ecs to cloudfront 
    new cloudfront.Distribution(this, 'myDist', {
      defaultBehavior: { 
        origin: new origins.LoadBalancerV2Origin(fargate.loadBalancer , {
          protocolPolicy : cloudfront.OriginProtocolPolicy.HTTP_ONLY,
        }), 
        originRequestPolicy: myOriginRequestPolicy,
        allowedMethods : cloudfront.AllowedMethods.ALLOW_ALL,
      },
    });


    //create database 
    const rdsInstance = new rds.DatabaseInstance(this, 'Instance', {
      engine: rds.DatabaseInstanceEngine.mysql({ version: rds.MysqlEngineVersion.VER_8_0_19, }),
      // optional, defaults to m5.large
      instanceType: ec2.InstanceType.of(ec2.InstanceClass.T2, ec2.InstanceSize.MICRO),
      allocatedStorage : 20 , 
      vpc,
      vpcSubnets: {
        subnetType: ec2.SubnetType.PRIVATE_ISOLATED,
      },
      securityGroups : [rdsSecurityGroup],
      backupRetention : Duration.days(7), 
      deleteAutomatedBackups : true,  
    });


    // example resource
    // const queue = new sqs.Queue(this, 'CdkEcsAlbRoute53Queue', {
    //   visibilityTimeout: cdk.Duration.seconds(300)
    // });
  }
}
